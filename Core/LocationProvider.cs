﻿using UnityEngine;

namespace Me.StefS.Utility{
	public interface LocationProvider {
		Vector3 Location { get; }
	}
}