﻿using System.Collections.Generic;
using Me.StefS.Utility;

namespace Me.StefS.Polis
{
	public class OrientationSideUtil {
		public static OrientationSide FindSingleMissingSide(ICollection<OrientationSide> theRestOfTheSides){
			if (theRestOfTheSides == null) {
				throw new System.ArgumentNullException("theRestOfTheSides");
			}
			
			HashSet<OrientationSide> missingSides = new HashSet<OrientationSide> (OrientationSideExtensions.GetValues ());
			missingSides.RemoveAll (theRestOfTheSides);
			if(missingSides.Count != 1){
				throw new System.InvalidOperationException("There should be exactly 1 missing side, and there is" + missingSides.Count);
			}
			
			OrientationSide missingSide = missingSides.First();
			return missingSide;
		}
	}
}
