﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using Me.StefS.Utility;


namespace Tests
{
    public class Vector3ExtensionsTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void RightXY_1x2y_2xmin1y()
        {
            //Arrange
            Vector3 orig = new Vector3(1, 2, 0);
            Vector3 expected = new Vector3(2, -1, 0);

            //Act
            Vector3 right = orig.RightXY();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void Right_1x2y_2xmin1y() {
            //Arrange
            Vector3 orig = new Vector3(1, 0, 2);
            Vector3 expected = new Vector3(2, 0, -1);

            //Act
            Vector3 right = orig.Right();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void RightXY_2xmin1y_min1xmin2y() {
            //Arrange
			Vector3 orig = new Vector3(2, -1, 0);
            Vector3 expected = new Vector3(-1, -2, 0);

            //Act
            Vector3 right = orig.RightXY();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void Right_2xmin1y_min1xmin2y() {
            //Arrange
			Vector3 orig = new Vector3(2, 0, -1);
            Vector3 expected = new Vector3(-1, 0, -2);

            //Act
            Vector3 right = orig.Right();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void LeftXY_2xmin1y_1x2y() {
            //Arrange
            Vector3 orig = new Vector3(2, -1, 0);
            Vector3 expected = new Vector3(1, 2, 0);

            //Act
            Vector3 right = orig.LeftXY();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void Left_2xmin1y_1x2y() {
            //Arrange
            Vector3 orig = new Vector3(2, 0, -1);
            Vector3 expected =new Vector3(1, 0, 2);

            //Act
            Vector3 right = orig.Left();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void LeftXY_min1xmin2y_2xmin1y() {
            //Arrange
            Vector3 orig = new Vector3(-1, -2, 0);
            Vector3 expected = new Vector3(2, -1, 0);
            //Act
            Vector3 right = orig.LeftXY();

            //Assert
            Assert.AreEqual(expected, right);
        }

        // A Test behaves as an ordinary method
        [Test]
        public void Left_min1xmin2y_2xmin1y() {
            //Arrange
            Vector3 orig = new Vector3(-1, 0, -2); 
            Vector3 expected = new Vector3(2, 0, -1);

            //Act
            Vector3 right = orig.Left();

            //Assert
            Assert.AreEqual(expected, right);
        }


        [Test]
        public void GetMatchingQuadrantDiagonal_firstQuadrant_upRight(){
            //Arrange
            Vector3 firstQadrant = new Vector3(2, 0, 3);

            //Act
            DiagonalOrientationSide actual = firstQadrant.GetMatchingQuadrantDiagonal();

            //Assert
            Assert.That(actual, Is.EqualTo(DiagonalOrientationSide.UpRight));
        }

        [Test]
        public void GetMatchingQuadrantDiagonal_fourthQuadrant_upLeft() {
            //Arrange
            Vector3 firstQadrant = new Vector3(2, 0, -3);

            //Act
            DiagonalOrientationSide actual = firstQadrant.GetMatchingQuadrantDiagonal();

            //Assert
            Assert.That(actual, Is.EqualTo(DiagonalOrientationSide.DownRight));
        }
    }
}
