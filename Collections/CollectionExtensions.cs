﻿using System.Collections.Generic;

namespace Me.StefS.Utility {
	public static class CollectionExtensions{
		public static ICollection<T> AddAll<T>(this ICollection<T> collection, IEnumerable<T> elements){
			if(collection == null){
				throw new System.ArgumentNullException("collection");
			}
			
			if(elements == null){
				throw new System.ArgumentNullException("elements");
			}
			
			IEnumerator<T> enumerator = elements.GetEnumerator();
			try{
				while(enumerator.MoveNext()){
					collection.Add(enumerator.Current);
				}
			}
			finally{
				enumerator.Dispose();
			}
			
			return collection;
		}
		
		public static ICollection<T> RemoveAll<T>(this ICollection<T> collection, IEnumerable<T> elements) {
			if(collection == null){
				throw new System.ArgumentNullException("collection");
			}
			
			if(elements == null){
				throw new System.ArgumentNullException("elements");
			}
			
			IEnumerator<T> enumerator = elements.GetEnumerator();
			try{
				while(enumerator.MoveNext()){
					collection.Remove(enumerator.Current);
				}
			}
			finally{
				enumerator.Dispose();
			}
			
			return collection;
		}
	}
}