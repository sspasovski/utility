﻿using System.Collections.Generic;

namespace Me.StefS.Utility{
	public static class Array {
		public static IList<int> GenerateIntegerGaps(int length, int numberOfGaps){
			IList<int> gaps = new int[numberOfGaps];
			int gapIndex = 0;
			while(length > 0){
				gaps[gapIndex] ++;
				length --;
				gapIndex = ++gapIndex % numberOfGaps;
			}

			return gaps;
		}
	}
}