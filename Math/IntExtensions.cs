﻿using UnityEngine;

namespace Me.StefS.Utility{
	public static class IntExtensions {
		public static Vector3 X(this int xCoordinateVector){
			return new Vector3(xCoordinateVector, 0, 0);
		}
		
		public static Vector3 Y(this int yCoordinateVector){
			return new Vector3(0, yCoordinateVector, 0);
		}

		public static Vector3 Z(this int zCoordinateVector){
			return new Vector3(0, 0, zCoordinateVector);
		}

		public static Vector3 XYZ(this int vector){
			return new Vector3(vector, vector, vector);
		}

		public static int Clamp(this int value, int min, int max){
			if(value > min){
				if(value < max){
					return value;
				}else{
					return max;
				}
			}else{
				return min;
			}
		}
	}
}