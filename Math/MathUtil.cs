﻿using System;

namespace Me.StefS.Utility{
	public static class MathUtil{
		public static float Towards(float value, float target, float step){
			if (step <= 0) {
				throw new ArgumentOutOfRangeException ("step");
			}


			value = value < target
				? UnityEngine.Mathf.Clamp(value+step, value, target)
				: UnityEngine.Mathf.Clamp(value-step, target, value);
			return value;
		}
	}
}

