﻿namespace Me.StefS.Utility{
	public static class Constants {
		public static float Epsilon = 0.0001f;
		public static float Pi = 3.14159265358979323846264338327950288419716939937510f;
		public static float SquareRootOfTwo = 1.41421356237309504880168872420969807856967187537694807317667973799f;
	}
}