﻿using UnityEngine;

namespace Me.StefS.Utility{
	public static class FloatExtensions {
		public static int SafeCastToInt(this float number){
			int integer = Mathf.RoundToInt (number);
			if(Mathf.Abs(number - integer) > Constants.Epsilon)
			{
				throw new System.ArgumentOutOfRangeException("The float value " + number + "is too much off to be safely cast to integer");
			}

			return integer;
		}

		public static Vector3 X(this float xCoordinateVector){
			return new Vector3(xCoordinateVector, 0, 0);
		}

		
		public static Vector3 Y(this float yCoordinateVector){
			return new Vector3(0, yCoordinateVector, 0);
		}

		
		public static Vector3 Z(this float zCoordinateVector){
			return new Vector3(0, 0, zCoordinateVector);
		}

	    public static Vector3 XYZ(this float vector){
	        return new Vector3(vector, vector, vector);
	    }
			
	    public static bool ApproximatelyEquals(this float firstValue, float secondValue, float epsilon)
		{
			return Mathf.Abs(firstValue - secondValue) <= epsilon;
		}
	}
}
